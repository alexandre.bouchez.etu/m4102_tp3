### API et représentation des données

API REST de la ressource *commandes* :

| Opération | URI         | Action réalisée                               | Retour                                        |
|:----------|:------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /commandes     | récupère l'ensemble des commandes          | 200 et une liste de commandes            |
| GET       | /commandes/{id} | récupère la commande d'identifiant id     | 200 et la commande                           |
|           |             |                                               | 404 si id est inconnu                         |
| GET       | /commandes/{id}/nom | récupère le nom de la personne ayant effectué la commande d'identifiant id  | 200 et le nom de la personne ayant effectué la commande              |
|           |             |                                               | 404 si id est inconnu                         |
| POST      | /commandes | création de la commande                          | 201 et l'URI de la ressource créée + représentation |
|           |             |                                               | 400 si les informations ne sont pas correctes |
|           |             |                                               | 409 si la commande existe déjà (même nom et prenom)    |
| DELETE    | /commandes/{id} | destruction de la commande d'identifiant id | 204 si l'opération à réussi                   |
|           |             |                                               | 404 si id est inconnu                         |


Une commande comporte un identifiant, un nom, un prénom et une liste de pizzas. 
Sa représentation JSON prendra donc la forme suivante :

	{
	"id": 1,
	"nom": "Bouchez", 
    "prenom": "Alexandre",
    "pizzas": ["Margherita","Chevre miel"]
	}
	
Lors de la création, l'identifiant n'est pas connu car il sera fourni
par la base de données. Aussi on aura une
représentation JSON qui comporte uniquement le nom, le prénom et les pizzas :

	{
	"nom": "Bouchez", 
    "prenom": "Alexandre",
    "pizzas": ["Margherita","Chevre miel"]
	}