package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface javax.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class CommandeResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(CommandeResourceTest.class.getName());
	private CommandeDao dao;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}

	// Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la base de données
	// et les DAO

	// https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(CommandeDao.class);
		dao.createCommandeAndAssociationTable();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.deleteCommandeAndAssociationTable();
	}

	@Test
	public void testGetEmptyList() {
		Response response = target("/commandes").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		List<CommandeDto> commande;
		commande = response.readEntity(new GenericType<List<CommandeDto>>(){});

		assertEquals(0, commande.size());
	}

	//DOIT PRENDRE EN COMPTE LE NOM ET LE PRENOM, PAS QUE LE NOM
	/*@Test
    public void testGetExistingCommande() {

            Commande commande = new Commande();
            commande.setNom("Bouchez");

            long id = dao.insert(commande.getNom());
            commande.setId(id);

            Response response = target("/commandes/" + id).request().get();

            assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

            Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
            assertEquals(commande, result);
    }*/

	@Test
	public void testGetNotExistingCommande() {
		Response response = target("/commandes/125").request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}

	@Test
	public void testGetNotExistingCommandeNom() {
		Response response = target("commandes/125/nom").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	/*@Test
	public void testGetCommandeNom() {
		Commande commande = new Commande();
		commande.setNom("Bouchez");
		long id = dao.insert(commande.getNom());

		Response response = target("commandes/" + id + "/nom").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Bouchez", response.readEntity(String.class));
	}*/
	

	/*@Test
	public void testDeleteExistingCommande() {
		Commande commande = new Commande();
		commande.setNom("Bouchez");
		long id = dao.insert(commande.getNom());
		commande.setId(id);

		Response response = target("/commandes/" + id).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Commande result = dao.findById(id);
		assertEquals(result, null);
	}*/

	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/commandes/125").request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}

	@Test
	public void testCreateCommandeWithoutNom() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();

		Response response = target("/commandes")
				.request()
				.post(Entity.json(commandeCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}


}
