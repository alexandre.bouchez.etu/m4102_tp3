package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.dto.*;

public class Commande {
	 private long id;
	 private String nom;
	 private String prenom;
	 private List<Pizza> pizzas;
	 
	 public Commande() {}
	 
	 public Commande(long id, String nom, String prenom, List<Pizza> pizzas) {
		 this.id = id;
		 this.nom = nom;
		 this.prenom = prenom;
		 this.pizzas = pizzas;
	 }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Pizza> getPizzas() {
		return pizzas;
	}

	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}

	public static CommandeDto toDto(Commande c) {
	      CommandeDto dto = new CommandeDto();
	      dto.setId(c.getId());
	      dto.setNom(c.getNom());
	      dto.setPrenom(c.getPrenom());
	      dto.setPizzas(c.getPizzas());
	      
	      return dto;
	    }
	  
	    public static Commande fromDto(CommandeDto dto) {
	      Commande commande = new Commande();
	      commande.setId(dto.getId());
	      commande.setNom(dto.getNom());
	      commande.setPrenom(dto.getPrenom());
	      commande.setPizzas(dto.getPizzas());
	      
	      return commande;
	    }
	    
	    @Override
	    public boolean equals(Object obj) {
	      if (this == obj)
	          return true;
	      if (obj == null)
	          return false;
	      if (getClass() != obj.getClass())
	          return false;
	      Commande other = (Commande) obj;
	      if (id != other.id)
	          return false;
	      if (nom == null) {
	          if (other.nom != null)
	              return false;
	      } else if (!nom.equals(other.nom))
	          return false;
	      if (prenom == null) {
	          if (other.prenom != null)
	              return false;
	      } else if (!prenom.equals(other.prenom))
	          return false;
	      return true;
	    }
	    
	    public static CommandeCreateDto toCreateDto(Commande commande) {
	        CommandeCreateDto dto = new CommandeCreateDto();
	        dto.setNom(commande.getNom());
	        dto.setPrenom(commande.getPrenom());
	        
	        return dto;
	    }

	    public static Commande fromCommandeCreateDto(CommandeCreateDto dto) {
	        Commande commande = new Commande();
	        commande.setNom(dto.getNom());
	        commande.setPrenom(dto.getPrenom());

	        return commande;
	    }

		@Override
		public String toString() {
			return "Commande [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", pizzas=" + pizzas + "]";
		}
	 
}
