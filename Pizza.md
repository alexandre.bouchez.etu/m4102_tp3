### API et représentation des données

API REST de la ressource *pizzas* :

| Opération | URI         | Action réalisée                               | Retour                                        |
|:----------|:------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /pizzas     | récupère l'ensemble des pizzas                | 200 et une liste de pizzas             |
| GET       | /pizzas/{id} | récupère la pizza d'identifiant id           | 200 et la pizza                           |
|           |             |                                               | 404 si id est inconnu                         |
| GET       | /pizzas/{id}/name | récupère le nom de la pizza d'identifiant id  | 200 et le nom de la pizza                 |
|           |             |                                               | 404 si id est inconnu                         |
| POST      | /pizzas | création de la pizza                          | 201 et l'URI de la ressource créée + représentation |
|           |             |                                               | 400 si les informations ne sont pas correctes |
|           |             |                                               | 409 si la pizza existe déjà (même nom)    |
| DELETE    | /pizzas/{id} | destruction de la pizza d'identifiant id | 204 si l'opération à réussi                   |
|           |             |                                               | 404 si id est inconnu                         |


Une pizza comporte un identifiant, un nom et une liste d'ingrédients. 
Sa représentation JSON prendra donc la forme suivante :

	{
	"id": 1,
	"name": "Margherita", 
	"ingredients": ["mozzarella","tomate"]
	}
	
Lors de la création, l'identifiant n'est pas connu car il sera fourni
par la base de données. Aussi on aura une
représentation JSON qui comporte uniquement le nom et les ingrédients :

	{
	"name": "Margherita", 
	"ingredients": ["mozzarella","tomate"]
	}